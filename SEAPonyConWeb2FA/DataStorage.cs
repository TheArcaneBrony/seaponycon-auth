﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace SEAPonyConWeb2FA
{
    class DataStorage
    {
        public static string BaseDir => RuntimeInformation.IsOSPlatform(OSPlatform.Linux) ? "/data/" : "Data/";
        public static DataStorage Load()
        {
            if (Directory.Exists(BaseDir)) if (File.Exists(BaseDir+"Config.json")) return JsonConvert.DeserializeObject<DataStorage>(File.ReadAllText(BaseDir+"Config.json"));
            return new DataStorage();
        }
        public void Save()
        {
            if (!Directory.Exists(BaseDir)) Directory.CreateDirectory(BaseDir);
            try
            {
                File.WriteAllText(BaseDir+"Config.json", JsonConvert.SerializeObject(this, Formatting.Indented));
                Console.WriteLine("Saved successfully!");
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine("Failed to save!");
            }

        }
        public Dictionary<string, User> Users = new Dictionary<string, User>();
    }
    class User
    {
        public string IP;
        public string Secret;
    }
}
