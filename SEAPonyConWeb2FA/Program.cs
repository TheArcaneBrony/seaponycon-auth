﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

using TwoFactorAuthNet;

namespace SEAPonyConWeb2FA
{
    class Program
    {
        static TcpListener server = new TcpListener(IPAddress.Any, 6969);
        static TwoFactorAuth tfa = new TwoFactorAuth("SEAPonyCon");
        static DataStorage dataStore = DataStorage.Load();
        static int threadcount = 32;
        public static void Main(string[] args)
        {
            server.Start();
            Console.WriteLine($"{GetThreadNumberPrefixFormatted(0)}Server has started on *:6969, Waiting for a connection...");

            for (int i = 0; i < threadcount; i++)
            {
                StartServerThread(i+1);
            }
            //Thread.Sleep(120000);
            while (true)
            {
                //Thread.Sleep(60000);
                Thread.Sleep(1000);
                dataStore.Save();
            }
        }

        private static Thread StartServerThread(int num)
        {
            Console.WriteLine($"{GetThreadNumberPrefixFormatted(0)}Starting thread {num}/{threadcount}");
            var thr = new Thread(() =>
            {
                ServerThread(num);
            });
            thr.Start();
            return thr;
        }
        private static void ServerThread(int num)
        {
            Console.WriteLine($"{GetThreadNumberPrefixFormatted(num)}Started thread {num}/{threadcount}");
            while (true)
            {
                TcpClient client = server.AcceptTcpClient();
                Console.WriteLine($"{GetThreadNumberPrefixFormatted(num)}[{DateTime.Now.ToLongTimeString()}] A client connected.");
                NetworkStream stream = client.GetStream();
                Stopwatch sw = new Stopwatch();
                sw.Start();
                while (sw.ElapsedMilliseconds < 100 && !stream.DataAvailable) ;
                while (sw.ElapsedMilliseconds < 100 && client.Available < 3) ; // match against "get"
                if (client.Available < 3)
                {
                    Console.WriteLine("{GetThreadNumberPrefixFormatted(num)}Not responding to client, too much time has gone by waiting for data!");
                    client.Close();
                }
                else
                    try
                    {
                        byte[] bytes = new byte[client.Available];
                        stream.Read(bytes, 0, client.Available);
                        string s = Encoding.UTF8.GetString(bytes);

                        if (Regex.IsMatch(s, "^GET", RegexOptions.IgnoreCase))
                        {
                            //Console.WriteLine("=====Handshaking from client=====\n{0}", s);

                            // HTTP/1.1 defines the sequence CR LF as the end-of-line marker
                            string url = s.Split(" HTTP/")[0].Replace("GET ", ""), resp = "HTTP/1.1 200 OK\r\nServer: SEAPonyCon/1.0\r\nContent-Type: text/html\r\n\r\n";
                            Dictionary<string, string> urlargs = new Dictionary<string, string>();
                            if (url.Contains('?') && url.Contains('='))
                            {
                                foreach (string kvp in url.Split("?")[1].Split("&")) urlargs.Add(kvp.Split('=')[0], kvp.Split('=')[1]);
                                url = url.Split('?')[0];
                            }


                            if (url == "/")
                            {
                                resp += "You have found the secret website!";
                            }
                            else if (url == "/generate")
                            {
                                if (!urlargs.ContainsKey("id")) resp += "Must provide variable: id!";
                                else if (dataStore.Users.ContainsKey(urlargs["id"])) resp += "User already exists!";
                                else
                                {
                                    var secret = tfa.CreateSecret(2048);
                                    //resp += JsonConvert.SerializeObject(urlargs, Formatting.Indented);//.Replace("\n","\r\n");
                                    try
                                    {
                                        resp += "<img src=\"" + tfa.GetQrCodeImageAsDataUri(urlargs["id"], secret, 512) + "\"><br>" + secret;
                                    }
                                    catch (WebException)
                                    {
                                        resp += "Failed to generate QR code, please try again later!";
                                    }
                                    try
                                    {
                                        dataStore.Users.Add(urlargs["id"], new User()
                                        {
                                            Secret = secret,
                                            IP = ((IPEndPoint)client.Client.RemoteEndPoint).Address.ToString()
                                        });
                                    }
                                    catch (ArgumentException)
                                    {
                                        Console.Write("Failed to add to db!");
                                        resp += "<br>This key might not work, please try again later/get a different id!";
                                    }

                                }
                            }
                            else if (url == "/generate.b64")
                            {
                                if (!urlargs.ContainsKey("id")) resp += "Must provide variable: id!";
                                else if (dataStore.Users.ContainsKey(urlargs["id"])) resp += "User already exists!";
                                else
                                {
                                    var secret = tfa.CreateSecret(2048);
                                    //resp += JsonConvert.SerializeObject(urlargs, Formatting.Indented);//.Replace("\n","\r\n");
                                    try
                                    {
                                        resp += tfa.GetQrCodeImageAsDataUri(urlargs["id"], secret, 512).Replace("data:image/png;base64,", "");
                                    }
                                    catch (WebException)
                                    {
                                        resp += "Failed to generate QR code, please try again later!";
                                    }
                                    try
                                    {
                                        dataStore.Users.Add(urlargs["id"], new User()
                                        {
                                            Secret = secret,
                                            IP = ((IPEndPoint)client.Client.RemoteEndPoint).Address.ToString()
                                        });
                                    }
                                    catch (ArgumentException)
                                    {
                                        Console.Write("Failed to add to db!");
                                        resp += "<br>This key might not work, please try again later/get a different id!";
                                    }
                                    catch (NullReferenceException)
                                    {
                                        Console.Write("Failed to add to DB! (null?)");
                                    }

                                }
                            }
                            else if (url == "/check")
                            {
                                if (!urlargs.ContainsKey("id")) resp += "Must provide variable: id!";
                                else if (!urlargs.ContainsKey("code")) resp += "Must provide variable: code!";
                                else if (!(dataStore.Users.ContainsKey(urlargs["id"]) && int.TryParse(urlargs["code"].Replace("%20", ""), out int a))) resp += "False";
                                else resp += tfa.VerifyCode(dataStore.Users[urlargs["id"]].Secret, urlargs["code"].Replace("%20", ""));
                            }
                            else if (url == "/recover")
                            {
                                if (!urlargs.ContainsKey("id")) resp += "Must provide variable: id!";
                                else if (!dataStore.Users.ContainsKey(urlargs["id"])) resp += "User does not exist!";
                                else if (dataStore.Users[urlargs["id"]].IP != ((IPEndPoint)client.Client.RemoteEndPoint).Address.ToString()) resp += "Registration IP must match current IP!";
                                else resp += "<img src=\"" + tfa.GetQrCodeImageAsDataUri(urlargs["id"], dataStore.Users[urlargs["id"]].Secret, 512) + "\"><br>" + dataStore.Users[urlargs["id"]].Secret;
                            }
                            else
                            {
                                resp = "HTTP/1.1 404 Not found\r\nServer: SEAPonyCon/1.0";
                            }

                            byte[] response = Encoding.UTF8.GetBytes(resp);
                            try
                            {
                                stream.Write(response, 0, response.Length);
                            }
                            catch (IOException)
                            {
                                Console.WriteLine($"{GetThreadNumberPrefixFormatted(num)}Failed to respond!");
                            }

                        }
                        stream.Close();
                    }
                    catch (IOException)
                    {
                        Console.WriteLine($"{GetThreadNumberPrefixFormatted(num)}Failed to read data from client!");
                    }
                sw.Stop();
            }

        }
        public static string GetThreadNumberPrefixFormatted(int num)
        {
            return ("" + num).PadLeft((threadcount + "").Length) + ": ";
        }
    }
}
